//
//  ComponentsViewDelegate.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import UIKit

protocol ComponentViewDelegate: class {
    // All components should call this function to add itself to viewControllers
    func add(view: UIView, withBlur: Bool)
    
    func center(view: UIView)
    
    func alignBottom(view: UIView)
    
    // This function only should called by blurComponents
    func removeBlurComponent(view: UIView)
}
