//
//  RepoTime.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class RepoTime {
    
    static func getTime(to location: Location, responder: IResponder) {
        Proxy<Time>.execute(url: Constants.API.FORECAST,
                            methodType: .get,
                            parameters: location,
                            headers: nil,
                            responder: responder,
                            serviceTag: Constants.ServiceTags.FORECAST)
    }
}
