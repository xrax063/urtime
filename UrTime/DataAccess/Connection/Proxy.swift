//
//  Proxy.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation
import Alamofire

class Proxy<T: BaseModel> {
    
    static func execute(url: String,
                        methodType: HTTPMethod,
                        parameters: BaseModel?,
                        headers: [String: String]?,
                        responder: IResponder,
                        serviceTag: Int)
    {
        if !Utils.isNetworkAvaible() {
            let objectResponse = Response(code: Constants.HttpCodes.UNEXPECTED_ERROR,
                                          message: Constants.ViewStrings.NOT_INTERNET_CONNECTION)
            responder.onFailedResponse(response: objectResponse,
                                       serviceTag: Constants.ServiceTags.NOT_INTERNET_CONNECTION)
            return
        }
        
        var body: [String: AnyObject]? = parameters?.toDictionary()

        // Initialize headers dictionary if is null
        let headers: [String: String]! = headers != nil ? headers : [String: String]()
        
        var urlPath = Constants.API.BASE + url
        if methodType == .get {
            urlPath = setParametersIn(url: urlPath,
                                      withQuery: body)
            body = nil
        }
        
        do {
            try Alamofire.request(urlPath.asURL(),
                                  method: methodType,
                                  parameters: body,
                                  encoding: JSONEncoding.default,
                                  headers: headers).responseJSON { response in
                                    processResponse(response: response,
                                                    responder: responder,
                                                    serviceTag: serviceTag)
            }
        } catch {
            let messageResponse = Response(code: Constants.HttpCodes.UNEXPECTED_ERROR,
                                           message: Constants.ViewStrings.URL_BUILD_ERROR)
            responder.onFailedResponse(response: messageResponse,
                                       serviceTag: serviceTag)
            return
        }
        
    }
    
    private static func setParametersIn(url: String,
                                        withQuery query: [String: AnyObject]?) -> String
    {
        var url = url
        if query == nil {
            return url
        }
        
        for queryParam in query! {
            url = url.replacingOccurrences(of: Constants.Keys.CHARSET,
                                           with: "\(queryParam.value)",
                                           range: url.range(of: Constants.Keys.CHARSET))
        }
        
        return url
    }
    
    private static func processResponse(response: DataResponse<Any>,
                                        responder: IResponder,
                                        serviceTag: Int)
    {
        let codeStatus = response.response?.statusCode
        let dictionary = response.result.value as? [String: AnyObject]
        let array = response.result.value as? [[String: AnyObject]]
        
        if codeStatus != Constants.HttpCodes.OK {
            
            let messageResponse = Response(code: codeStatus!,
                                           message: Constants.ViewStrings.ERROR_INVALID_RESPONSE)
            responder.onFailedResponse(response: messageResponse,
                                       serviceTag: serviceTag)
            
            return
        }
        
        // If no has data
        if dictionary == nil && array == nil {
            let nilObject: Response? = nil
            responder.onSuccessResponse(object: nilObject, serviceTag: serviceTag)
            return
        }
        
        // If have an object
        if dictionary != nil {
            let modelResponse = T()
            modelResponse.fromDictionary(dictionary!)
            responder.onSuccessResponse(object: modelResponse, serviceTag: serviceTag)
            return
        }
        
        // If have an array
        if array != nil {
            let dataArrayResponse = ArrayResponse(objectsArray: array!)
            responder.onSuccessResponse(object: dataArrayResponse, serviceTag: serviceTag)
            return
        }
        
        // If response is not a valid JSON
        let fallidResponse = Response(code: Constants.HttpCodes.UNEXPECTED_ERROR,
                                      message: Constants.ViewStrings.ERROR_INVALID_RESPONSE)
        
        responder.onFailedResponse(response: fallidResponse, serviceTag: serviceTag)
    }
}
