//
//  IResponder.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

protocol IResponder {
    func onSuccessResponse<T: BaseModel>(object: T?, serviceTag: Int)
    func onFailedResponse(response: Response, serviceTag: Int)
}
