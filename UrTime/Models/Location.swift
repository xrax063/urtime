//
//  Location.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation
import CoreLocation

class Location: BaseModel {
    
    var latitude: NSNumber!
    var longitude: NSNumber!
    
    required init() {}
    
    // Constructor to get Currently by lat and long
    init(at location: CLLocation) {
        super.init()
        self.latitude = location.coordinate.latitude as NSNumber
        self.longitude = location.coordinate.longitude as NSNumber
    }
}
