//
//  BaseModel.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation
import LABParser

open class BaseModel: ParcelableModel {
    
    open override func customKeysName() -> [String : String]? {
        return nil
    }
}
