//
//  Currently.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class Currently: BaseModel {
    
    var summary: String!
    var icon: String!
    var precipProbability: NSNumber!
    var temperature: NSNumber!
    var humidity: NSNumber!
    
}
