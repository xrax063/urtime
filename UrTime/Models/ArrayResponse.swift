//
//  ArrayResponse.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class ArrayResponse: BaseModel {
    
    // MARK: Properties
    
    var objectsArray: [[String: AnyObject]]
    
    required init() {
        objectsArray = [[String: AnyObject]]()
    }
    
    init(objectsArray: [[String: AnyObject]]) {
        self.objectsArray = objectsArray
    }
    
    // MARK: Actions
    
    func appendElement(_ element: [String: AnyObject]) {
        objectsArray.append(element)
    }
}
