//
//  Time.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class Time: Location {
    
    var timezone: String!
    var currently: Currently!
    
    required init() {
        super.init()
    }
}
