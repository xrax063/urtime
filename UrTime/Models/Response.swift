//
//  Response.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class Response: BaseModel {
    var code: Int!
    var message: String!
    
    required init() {}
    
    init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
}
