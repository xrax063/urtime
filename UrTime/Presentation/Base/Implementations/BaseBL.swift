//
//  BaseBL.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class BaseBL: IBaseBL {
    
    internal weak var listener: IBaseListener!
    
    required init(baseListener: IBaseListener) {
        self.listener = baseListener
    }
    
    func onSuccessResponse<T>(object: T?, serviceTag: Int) where T : BaseModel {
    }
    
    func onFailedResponse(response: Response, serviceTag: Int) {
        listener.onFailedResponse(response, serviceTag: serviceTag)
    }
}
