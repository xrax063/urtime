//
//  BasePresenter.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import Foundation

class BasePresenter: NSObject {
    internal var bl: IBaseBL!
    internal weak var view: IBaseView!
    
    required init(baseView: IBaseView) {
        self.view = baseView
    }
}

extension BasePresenter: IBasePresenter{
    
}
extension BasePresenter: IBaseListener{
    func onFailedResponse(_ response: Response, serviceTag: Int) {
        view.notifyFailedResponse(response, serviceTag: serviceTag)
    }
}
