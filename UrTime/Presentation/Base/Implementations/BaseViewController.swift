//
//  BaseView.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate, ComponentViewDelegate, AlertViewDelegate, ProgressViewDelegate, IBaseView
{
    internal var presenter: IBasePresenter!
    var popoverContentView: UIPopoverPresentationController?
    var alertView: AlertView?
    var progressView: ProgressView!
    var blurEffectViews = [UIVisualEffectView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func formdDone() {
        //This function must be overwritten
    }
    
    internal func showAlert(alertType: Constants.AlertTypes = .INFO,
                            title: String = Constants.ViewStrings.INFO_TITLE,
                            message: String,
                            positiveButton: String = Constants.ViewStrings.OK,
                            negativeButton: String? = nil,
                            tag: Constants.AlertTags = .DEFAULT_OK,
                            delegate: AlertViewDelegate)
    {
        var icAlert: UIImage!
        
        switch alertType {
        case Constants.AlertTypes.DONE:
            icAlert = #imageLiteral(resourceName: "icDone")
            break
        case Constants.AlertTypes.INFO:
            icAlert = #imageLiteral(resourceName: "icInfo")
            break
        case Constants.AlertTypes.ERROR:
            icAlert = #imageLiteral(resourceName: "icError")
            break
        }
        
        DispatchQueue.main.async {
            self.hideProgress()
            self.alertView = AlertView(icAlert: icAlert,
                                       title: title,
                                       message: message,
                                       positiveButton: positiveButton,
                                       negativeButton: negativeButton,
                                       tag: tag,
                                       delegate: delegate)
        }
    }
    
    /**
     Show a progressView
     */
    internal func showProgress(withTitle title: String? = nil) {
        self.view.endEditing(true)
        if progressView == nil {
            progressView = ProgressView(delegate: self)
        }
        progressView.show(title)
    }
    
    /**
     Hide the current progressView if is child of current parent view.
     */
    internal func hideProgress() {
        DispatchQueue.main.async {
            if self.progressView != nil {
                self.progressView.hide()
                self.progressView = nil
            }
        }
    }
    
    // MARK: ComponentViewDelegate
    // All components should call this function to add itself to viewControllers
    func add(view: UIView, withBlur: Bool) {
        if withBlur {
            let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.alpha = 0
            blurEffectView.tag = Constants.BlurEffectTags.SHOW
            blurEffectViews.append(blurEffectView)
            self.view.addSubview(blurEffectView)
        }
        
        for subview in self.view.subviews where subview.isSameTypeOf(object: view) {
            subview.removeFromSuperview()
        }
        
        view.alpha = 0
        self.view.addSubview(view)
        
        // Si hay un AlertView actualmente se muestra por encima
        for alertView in self.view.subviews where alertView is AlertView {
            self.view.bringSubview(toFront: alertView)
        }
        
        UIView.animate(withDuration: 0.3,
                       animations:{
                        view.alpha = 1
                        for blurEffectView in self.blurEffectViews where blurEffectView.tag == Constants.BlurEffectTags.SHOW {
                            blurEffectView.alpha = 1
                        }
        },
                       completion: nil)
    }
    
    func center(view: UIView) {
        view.center = self.view.center
    }
    
    func alignBottom(view: UIView) {
        view.frame.origin.y = self.view.frame.height - view.frame.height
    }
    
    // This function only should called by blurComponents
    func removeBlurComponent(view: UIView) {
        UIView.animate(withDuration: 0.3,
                       animations: {
                        view.alpha = 0
                        self.blurEffectViews.last?.alpha = 0
                        self.blurEffectViews.last?.tag = Constants.BlurEffectTags.DROP
        }, completion: { _ in
            view.removeFromSuperview()
            for blurEffectView in self.blurEffectViews where blurEffectView.tag == Constants.BlurEffectTags.DROP {
                blurEffectView.removeFromSuperview()
                self.blurEffectViews.remove(at: self.blurEffectViews.index(of: blurEffectView)!)
            }
        })
    }
    
    // MARK: AlertViewDelegate
    func onOkClick(_ alertTag: Constants.AlertTags) {
        alertView = nil
    }
    
    func onCancelClick(_ alertTag: Constants.AlertTags) {
        alertView = nil
    }
    
    // MARK: UITtextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTf = self.view.viewWithTag(textField.tag + 1) {
            nextTf.becomeFirstResponder()
        } else {
            self.formdDone()
        }
        
        return true
    }
    
    /**
     Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: IBaseView
    func notifyFailedResponse(_ response: Response, serviceTag: Int) {
        hideProgress()
        
        showAlert(alertType: .ERROR,
                  title: Constants.ViewStrings.ERROR_TITLE,
                  message: response.message,
                  positiveButton: Constants.ViewStrings.OK,
                  delegate: self)
    }
}
