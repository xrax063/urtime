//
//  TimePresenter.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright (c) 2018 Leonardo Armero Barbosa. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class TimePresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ITimeBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ITimeView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ITimeView) {
        super.init(baseView: view)
        self.bl = TimeBL(listener: self)
    }
    
    fileprivate func getView() -> ITimeView {
        return view as! ITimeView
    }
    
    fileprivate func getBL() -> ITimeBL {
        return bl as! ITimeBL
    }
}

// MARK: Presenter
extension TimePresenter: ITimePresenter {

    // MARK: Actions
    func getTime(toLocation location: Location) {
        getBL().getTime(toLocation: location)
    }
}

// MARK: Listener
extension TimePresenter: ITimeListener {

    // MARK: Listeners
    
    func onTimeReceived(_ time: Time) {
        getView().updateTime(time)
    }
}

