//
//  TimeBL.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright (c) 2018 Leonardo Armero Barbosa. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class TimeBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ITimeListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ITimeListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ITimeListener {
        return listener as! ITimeListener
    }
    
    // MARK: Listeners
    
    override func onSuccessResponse<T>(object: T?, serviceTag: Int) where T : BaseModel {
        super.onSuccessResponse(object: object, serviceTag: serviceTag)
        
        switch serviceTag {
        case Constants.ServiceTags.FORECAST:
            getListener().onTimeReceived(object as! Time)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension TimeBL : ITimeBL {
    
    // MARK: Actions
    
    func getTime(toLocation location: Location) {
        RepoTime.getTime(to: location, responder: self)
    }
}
