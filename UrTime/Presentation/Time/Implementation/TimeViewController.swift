//
//  TimeViewController.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright (c) 2018 Leonardo Armero Barbosa. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit
import CoreLocation

// MARK: Base
class TimeViewController: BaseViewController {

    // MARK: Outlets
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var tvSummary: UITextView!
    @IBOutlet weak var lbPrecipitationProbability: UILabel!
    @IBOutlet weak var lbTemperature: UILabel!
    @IBOutlet weak var lbHumidity: UILabel!
    
  	// MARK: Properties
    private var locManager : CLLocationManager!
    private var lastLocation: CLLocation!
    private var lastCLAuthorizationStatus: CLAuthorizationStatus = .notDetermined

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ITimePresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ITimePresenter {
        return presenter as! ITimePresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = TimePresenter(view: self)
        
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
        
        // Check if the user allowed authorization
        if !verifyLocationServices() {
            return
        }
        
        showProgress()
        getTime()
    }
    
    private func verifyLocationServices() -> Bool {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus == .authorizedWhenInUse ||
            authorizationStatus == .authorizedAlways
        {
            return true
        } else if authorizationStatus == .notDetermined {
            locManager.requestWhenInUseAuthorization()
            return false
        }
        
        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!,
                                  options: [:],
                                  completionHandler: nil)
        return false
    }

    // MARK: Actions
    
    private func getTime() {
        guard let coordinates = locManager.location else {
            return
        }
        
        DispatchQueue.global().async {
            self.getPresenter().getTime(toLocation: Location(at: coordinates))
        }
    }
}

// MARK: View
extension TimeViewController : ITimeView {
    func updateTime(_ time: Time) {
        hideProgress()
//        ivIcon = algo
        tvSummary.text = time.currently.summary
        lbPrecipitationProbability.text = String(describing: time.currently.precipProbability.floatValue)
        lbTemperature.text = String(describing: time.currently.temperature.floatValue)
        lbHumidity.text = String(describing: time.currently.humidity.floatValue)
    }
}

extension TimeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getTime()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.last == nil {
            return
        }
        
        if lastLocation != nil && locations.last!.isSamePlaceTo(location: lastLocation)
        {
            return
        }

        lastLocation = locations.last!
        
        getTime()
    }
}
