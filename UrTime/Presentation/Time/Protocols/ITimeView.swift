//
//  ITimeView.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 1/03/18.
//  Copyright (c) 2018 Leonardo Armero Barbosa. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: View.

protocol ITimeView: IBaseView {
    func updateTime(_ time: Time)
}
