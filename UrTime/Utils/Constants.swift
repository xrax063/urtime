//
//  Constants.swift
//  UrTime
//
//  Created by Leonardo Armero Barbosa on 3/1/18.
//  Copyright © 2018 Leonardo Armero Barbosa. All rights reserved.
//

import UIKit

struct Constants {
    
    struct API {
        static let KEY: String = "03c3ac2e7b79c1b365deb6ee95305548"
        static let BASE: String = "https://api.darksky.net/"
        static let FORECAST: String = "forecast/\(KEY)/\(Keys.CHARSET),\(Keys.CHARSET)"
    }
    
    struct ServiceTags {
        static let NOT_INTERNET_CONNECTION: Int = 0
        static let FORECAST: Int = 1
    }
    
    struct Keys {
        static let CHARSET: String = "≠"
    }
    
    struct HttpCodes {
        static let OK: Int = 200
        static let BAD_REQUEST: Int = 400
        static let UNAUTHORIZED: Int = 401
        static let UNEXPECTED_ERROR: Int = 800
    }
    
    struct ViewStrings {
        static let ERROR_INVALID_RESPONSE: String = "Unexpected answer. Try again."
        static let URL_BUILD_ERROR: String = "The url cannot be converted"
        static let NOT_INTERNET_CONNECTION: String = "Device is not connected to internet."
        static let INFO_TITLE: String = "INFO"
        static let OK: String = "Ok"
        static let ERROR_TITLE: String = "Error"
    }
    
    struct Sizes {
        static let SCREEN: CGRect = UIScreen.main.bounds
    }
    
    struct Colors {
        static let PRIMARY: UIColor = UIColor(hex: "0092E7")
        static let ACCENT: UIColor = .white
        static let TINT: UIColor = .black
    }
    
    enum AlertTags {
        case DEFAULT_OK
    }
    
    enum AlertTypes {
        case DONE
        case ERROR
        case INFO
    }
    
    enum BlurEffectTags {
        static let DROP: Int = 0
        static let SHOW: Int = 1
    }
}
